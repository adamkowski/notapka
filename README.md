# Notapka

Simple notepad application written in Java with the client in the form of an Android application and communication via
HTTP with the backend server.

## Table of Contents

[[_TOC_]]

## About the project

The aim of the project was to create a client application that communicates with the server using security measures such
as token authentication. The main assumption was to use different technologies to present their usefulness for
professional applications in real projects.

For the sake of brevity, the repository contains two separate projects (backend and mobile). However, both projects are
managed by independent Gradle configurations and are built separately.

## Development

The project is built incrementally in stages. The functionalities of individual projects are created on separate
branches, which are transferred to develop. When the stage is over, each merge develop into master represents new
functionality and quality in the project.

1. **July 2020 - Structure of projects**
    - REST API with basic functionality endpoints
    - Communication of the mobile application with the server
    - Data persistence
2. **October 2020 - Application with user login functionality**
    - User registration and login
    - API authentication using Basic Auth
    - Useful Android application with layout for all functionalities
3. **May 2021 - JSON Web Token**
    - API authentication using JWT
    - Integration tests of all API functionalities
    - Liquibase for database management
    - API documented in the OpenAPI 3.0 specification, providing Swagger UI
    - Adaptation of the mobile application for token authentication

## Build

### Backend API

#### Preconditions

```
1. Java 11
2. PostgreSQL with created databases named:
   - notapka_prod
   - notapka_dev
   - notapka_test
```

#### Run application (prod)

```
gradle bootRun
```

#### Run application (dev)

```
gradle bootRun --args='--spring.profiles.active=development'
```

Using this profile gives you access to documentation at `<server>:<port>/swagger-ui.html`

#### Run tests

```
gradle test
```

After completing the task, the report is available under the
path `<project_dir>/backend/build/reports/tests/test/index.html`

### Android application

#### Preconditions

```
1. Android SDK
```

#### Build apk

To work properly, the application must have access to a connection with the server. Before starting the application,
make sure that the device or emulator is connected to the Internet. In the gradle configuration file of the android
application, enter the address where the server address `app/app/build.gradle` (android > buildTypes > debug >
buildConfigField).

```
echo "<path_to_sdk_disk_location>" > app/local.properties
gradle assembleDebug
```

After completing the task, the apk file is available under the path `<project_dir>/app/app/build/outputs/apk/debug`

## Technology

### Backend

Implemented with the use of the Spring Boot framework in Java 11. Its functioning is based on user authentication and
ensuring data persistence in the database on the server.

#### Spring Boot

The application was built in a structure with separate controllers, services, and a data persistence layer. The backend
part consists of REST API providing services for client applications in the form of registration, login and business
operations on notes.

The application runs in three profiles:

1. Production - database schema, data is persisted
2. Development - initialization with test data and stores records created during development
3. Test - test data only, intended for integration testing

Profiles are configured to run on different ports and separate PostgreSQL databases. With this solution, the application
instances can be run independently at the same time and do not affect the running of the tests.

#### Security

Authentication of individual users' access to the API is ensured by the use of JWT. The security measures were created
on the basis of mechanisms provided by Spring Security. Access to individual endpoints is controlled in the form of
Role-based Authorization. User data is secured, for example, by hashing passwords stored in the database.

#### Database

Liquibase is used to version the database schema and create the structure. Relational PostgreSQL database was selected
for the project, however Spring Data mechanisms provide abstraction for operations performed on the database. Class
consistency verification with real tables in the database is provided by Hibernate.

#### Documentation

All endpoints, transmitted data formats and returned HTTP codes have been documented using the OpenAPI 3.0
Specification. The project provides a Swagger UI that is configured to work with JWT security endpoints.

#### Tests

Integration tests have been created to ensure the correct functioning of all individual endpoints. A separate profile is
provided on which tests are run by default. Thanks to this solution, all functionalities are tested on separate
databases in the same PostgreSQL and have no negative impact on the functioning of the system.

### Android Application

A mobile application developed using Java 8. It allows you to store and fetching saved users data from the server.
Communication takes place in the background using Async Tasks. Layout created with the use of Material Design elements.
Adapter and RecyclerView were used to optimally display the list of notes.

## Author

Daniel Adamkowski

package com.gitlab.adamkowski.notapka.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.auth0.android.jwt.DecodeException;
import com.auth0.android.jwt.JWT;
import com.gitlab.adamkowski.notapka.R;
import com.gitlab.adamkowski.notapka.activities.notes.NotesScreenActivity;
import com.gitlab.adamkowski.notapka.activities.user.LoginUserActivity;

public class MainActivity extends AppCompatActivity {
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isUserLogged()) {
            openNotesScreen();
        } else {
            openLoginForm();
        }
    }

    private boolean isUserLogged() {
        try {
            JWT token = new JWT(sharedPreferences.getString("token", ""));
            boolean expired = token.isExpired(0);
            if (expired) {
                removeExpiredToken();
                Toast.makeText(this, "Login has expired", Toast.LENGTH_LONG).show();
                return false;
            }
        } catch (DecodeException e) {
            return false;
        }
        return true;
    }

    private void removeExpiredToken() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("token");
        editor.apply();
    }

    private void openLoginForm() {
        startActivity(new Intent(getApplicationContext(), LoginUserActivity.class));
    }

    private void openNotesScreen() {
        startActivity(new Intent(getApplicationContext(), NotesScreenActivity.class));
    }
}

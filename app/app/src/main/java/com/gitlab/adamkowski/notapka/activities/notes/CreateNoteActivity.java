package com.gitlab.adamkowski.notapka.activities.notes;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.gitlab.adamkowski.notapka.R;
import com.gitlab.adamkowski.notapka.models.NoteDTO;
import com.gitlab.adamkowski.notapka.network.SaveNoteTask;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.net.URL;

import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.API_NOTES;
import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.API_SERVER_ADDRESS;
import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.makeUrl;

public class CreateNoteActivity extends AppCompatActivity {
    private SharedPreferences sharedPreferences;
    private TextInputEditText title;
    private TextInputEditText content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_note_layout);

        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);

        title = findViewById(R.id.et_title);
        content = findViewById(R.id.et_content);

        MaterialButton buttonSave = findViewById(R.id.btn_save_note);
        buttonSave.setOnClickListener(view -> saveNewNote());
    }

    private void saveNewNote() {
        if (isTitleNotProvided()) {
            Toast.makeText(getApplicationContext(), "Title cannot be empty", Toast.LENGTH_SHORT).show();
        }
        URL url = makeUrl(API_SERVER_ADDRESS, API_NOTES);
        new SaveNoteTask(getNote(), getToken(), this).execute(url);
    }

    private boolean isTitleNotProvided() {
        if (title == null) {
            return true;
        }
        if (title.getText() == null) {
            return true;
        }
        return title.getText().toString().isEmpty();
    }

    private String getToken() {
        return sharedPreferences.getString("token", "");
    }

    private NoteDTO getNote() {
        return new NoteDTO(getNoteField(title), getNoteField(content));
    }

    private String getNoteField(TextInputEditText field) {
        if (field != null && field.getText() != null) {
            return field.getText().toString();
        }
        return "";
    }
}
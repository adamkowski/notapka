package com.gitlab.adamkowski.notapka.activities.notes;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.gitlab.adamkowski.notapka.R;
import com.gitlab.adamkowski.notapka.models.Note;
import com.gitlab.adamkowski.notapka.models.NoteDTO;
import com.gitlab.adamkowski.notapka.network.DeleteNoteTask;
import com.gitlab.adamkowski.notapka.network.EditNoteTask;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.net.URL;

import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.API_NOTES;
import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.API_SERVER_ADDRESS;
import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.makeUrl;

public class EditNoteActivity extends AppCompatActivity {
    private TextInputEditText inputEditTitle;
    private TextInputEditText inputEditContent;
    private SharedPreferences sharedPreferences;
    private Long noteId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_note_layout);

        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);

        this.inputEditTitle = findViewById(R.id.et_title);
        this.inputEditContent = findViewById(R.id.et_content);

        if (getIntent().getExtras() != null) {
            fillInputsWithNoteData(getIntent().getParcelableExtra("note"));
        }

        MaterialButton saveChangesButton = findViewById(R.id.btn_save_note);
        saveChangesButton.setOnClickListener(view -> saveNote());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int clickedItem = item.getItemId();
        if (clickedItem == R.id.action_delete_note) {
            URL deleteNoteUrl = makeUrl(API_SERVER_ADDRESS, API_NOTES, String.valueOf(noteId));
            new DeleteNoteTask(getLoggedUser(), this).execute(deleteNoteUrl);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void fillInputsWithNoteData(Note note) {
        if (note != null) {
            this.noteId = note.getId();
            this.inputEditTitle.setText(note.getTitle());
            this.inputEditContent.setText(note.getContent());
        }
    }

    private void saveNote() {
        if (isTileNotProvided()) {
            Toast.makeText(getApplicationContext(), "Title cannot be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        String token = getLoggedUser();
        NoteDTO note = getEditedNote();
        URL url = makeUrl(API_SERVER_ADDRESS, API_NOTES, String.valueOf(noteId));
        new EditNoteTask(token, note, this).execute(url);
    }

    private boolean isTileNotProvided() {
        return inputEditTitle.getText() == null || inputEditTitle.getText().toString().isEmpty();
    }

    private String getLoggedUser() {
        return sharedPreferences.getString("token", "");
    }

    private NoteDTO getEditedNote() {
        String title = extractText(inputEditTitle.getText());
        String content = extractText(inputEditContent.getText());
        return new NoteDTO(title, content);
    }

    private String extractText(Editable editedInput) {
        if (editedInput != null) {
            return editedInput.toString();
        }
        return "";
    }
}

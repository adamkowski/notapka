package com.gitlab.adamkowski.notapka.activities.notes;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.auth0.android.jwt.DecodeException;
import com.auth0.android.jwt.JWT;
import com.gitlab.adamkowski.notapka.R;
import com.gitlab.adamkowski.notapka.activities.user.UserSettingsActivity;
import com.gitlab.adamkowski.notapka.adapters.NoteAdapter;
import com.gitlab.adamkowski.notapka.interfaces.NoteAdapterOnClickHandler;
import com.gitlab.adamkowski.notapka.models.Note;
import com.gitlab.adamkowski.notapka.network.FetchNotesTask;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.API_NOTES;
import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.API_SERVER_ADDRESS;
import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.makeUrl;

public class NotesScreenActivity extends AppCompatActivity implements NoteAdapterOnClickHandler {
    private SharedPreferences sharedPreferences;
    private NoteAdapter noteAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_screen);

        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        noteAdapter = new NoteAdapter(this);

        FloatingActionButton fab = findViewById(R.id.notes_screen_fab_add_note);
        fab.setOnClickListener(view -> openCreatingNoteScreen());

        RecyclerView noteRecyclerView = findViewById(R.id.notes_screen_rv_notes);
        configureRecyclerView(noteRecyclerView);

        loadNotes();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isUserLogged()) {
            loadNotes();
        } else {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_user_settings) {
            startActivity(new Intent(getApplicationContext(), UserSettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(Note oneNote) {
        Intent intent = new Intent(getApplicationContext(), EditNoteActivity.class);
        intent.putExtra("note", oneNote);
        startActivity(intent);
    }

    private void openCreatingNoteScreen() {
        startActivity(new Intent(getApplicationContext(), CreateNoteActivity.class));
    }

    private void configureRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(noteAdapter);
    }

    private boolean isUserLogged() {
        try {
            JWT token = new JWT(sharedPreferences.getString("token", ""));
            boolean expired = token.isExpired(0);
            if (expired) {
                removeExpiredToken();
                Toast.makeText(this, "Login has expired", Toast.LENGTH_LONG).show();
                return false;
            }
        } catch (DecodeException e) {
            return false;
        }
        return true;
    }

    private void removeExpiredToken() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("token");
        editor.apply();
    }

    private void loadNotes() {
        String token = sharedPreferences.getString("token", "");
        new FetchNotesTask(noteAdapter, token).execute(makeUrl(API_SERVER_ADDRESS, API_NOTES));
    }
}
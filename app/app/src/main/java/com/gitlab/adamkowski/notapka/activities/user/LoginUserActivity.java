package com.gitlab.adamkowski.notapka.activities.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.gitlab.adamkowski.notapka.R;
import com.gitlab.adamkowski.notapka.models.User;
import com.gitlab.adamkowski.notapka.network.LoginUserTask;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.net.URL;

import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.API_LOGIN;
import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.API_SERVER_ADDRESS;
import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.makeUrl;

public class LoginUserActivity extends AppCompatActivity {
    private TextInputEditText inputUsername;
    private TextInputEditText inputPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_user);

        inputUsername = findViewById(R.id.login_input_username);
        inputPassword = findViewById(R.id.login_input_password);

        MaterialButton loginButton = findViewById(R.id.login_button);
        loginButton.setOnClickListener(view -> loginUser());

        TextView tvOpenRegistrationForm = findViewById(R.id.login_tv_register);
        tvOpenRegistrationForm.setOnClickListener(view -> openRegistrationForm());
    }

    private void loginUser() {
        boolean validForm = validateForm();
        if (validForm) {
            createLoginRequest();
        } else {
            inputPassword.getEditableText().clear();
        }
    }

    private void createLoginRequest() {
        String username = inputUsername.getEditableText().toString();
        String password = inputPassword.getEditableText().toString();
        User user = new User(username, password);
        URL url = makeUrl(API_SERVER_ADDRESS, API_LOGIN);
        new LoginUserTask(user, this, getSharedPreferences("user", Context.MODE_PRIVATE)).execute(url);
    }

    private boolean validateForm() {
        boolean validUsername = validateUsername();
        boolean validPassword = validatePassword();
        return validUsername && validPassword;
    }

    private boolean validateUsername() {
        TextInputLayout username = findViewById(R.id.login_username);
        if (isFilled(inputUsername)) {
            username.setError(null);
            username.setErrorEnabled(false);
            return true;
        } else {
            username.setError("Username can't be empty");
            return false;
        }
    }

    private boolean validatePassword() {
        TextInputLayout password = findViewById(R.id.login_password);
        if (isFilled(inputPassword)) {
            password.setError(null);
            password.setErrorEnabled(false);
            return true;
        } else {
            password.setError("Password can't be empty");
            return false;
        }
    }

    private boolean isFilled(TextInputEditText input) {
        String content = input.getEditableText().toString();
        return !TextUtils.isEmpty(content);
    }

    private void openRegistrationForm() {
        startActivity(new Intent(getApplicationContext(), RegisterUserActivity.class));
    }
}
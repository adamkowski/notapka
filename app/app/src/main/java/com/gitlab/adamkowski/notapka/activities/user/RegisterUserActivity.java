package com.gitlab.adamkowski.notapka.activities.user;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.gitlab.adamkowski.notapka.R;
import com.gitlab.adamkowski.notapka.models.User;
import com.gitlab.adamkowski.notapka.network.RegisterUserTask;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.net.URL;
import java.util.Locale;
import java.util.Objects;

import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.API_REGISTER;
import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.API_SERVER_ADDRESS;
import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.makeUrl;

public class RegisterUserActivity extends AppCompatActivity {
    private static final int USERNAME_MIN_LENGTH = 6;
    private static final int USERNAME_MAX_LENGTH = 32;
    private static final int PASSWORD_MIN_LENGTH = 6;
    private static final int PASSWORD_MAX_LENGTH = 16;

    private TextInputEditText inputUsername;
    private TextInputEditText inputPassword;
    private TextInputEditText inputConfirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        inputUsername = findViewById(R.id.register_input_username);
        inputPassword = findViewById(R.id.register_input_password);
        inputConfirmPassword = findViewById(R.id.register_input_confirm);

        MaterialButton registrationButton = findViewById(R.id.register_button);
        registrationButton.setOnClickListener(view -> registerUser());
    }

    private void registerUser() {
        boolean valid = validateForm();
        if (valid) {
            createRegistrationRequest();
        } else {
            clearPasswords();
        }
    }

    private void createRegistrationRequest() {
        User user = createUser();
        URL url = makeUrl(API_SERVER_ADDRESS, API_REGISTER);
        RegisterUserTask registerUserTask = new RegisterUserTask(user, this);
        registerUserTask.execute(url);
    }

    private User createUser() {
        String username = inputUsername.getEditableText().toString();
        String password = inputPassword.getEditableText().toString();
        return new User(username, password);
    }

    private void clearPasswords() {
        inputPassword.getEditableText().clear();
        inputConfirmPassword.getEditableText().clear();
    }

    private boolean validateForm() {
        boolean validUsername = validateUsername();
        boolean validPassword = validatePassword();
        boolean validConfirm = validateConfirmation();
        return validUsername && validPassword && validConfirm;
    }

    private boolean validateUsername() {
        TextInputLayout username = findViewById(R.id.register_username);
        if (isUsernameInvalid()) {
            String message = String.format(Locale.getDefault(),
                    "Username must be %d to %d characters long",
                    USERNAME_MIN_LENGTH,
                    USERNAME_MAX_LENGTH);
            username.setError(message);
            return false;
        } else {
            username.setError(null);
            username.setErrorEnabled(false);
            return true;
        }
    }

    private boolean isUsernameInvalid() {
        String username = inputUsername.getEditableText().toString();
        int length = username.trim().length();
        return length < USERNAME_MIN_LENGTH || length > USERNAME_MAX_LENGTH;
    }

    private boolean validatePassword() {
        TextInputLayout password = findViewById(R.id.register_password);
        if (isPasswordInvalid()) {
            String message = String.format(Locale.getDefault(),
                    "Password must be %d to %d characters long",
                    PASSWORD_MIN_LENGTH,
                    PASSWORD_MAX_LENGTH);
            password.setError(message);
            return false;
        } else {
            password.setError(null);
            password.setErrorEnabled(false);
            return true;
        }
    }

    private boolean isPasswordInvalid() {
        String password = inputPassword.getEditableText().toString();
        int length = password.length();
        return length < PASSWORD_MIN_LENGTH || length > PASSWORD_MAX_LENGTH;
    }

    private boolean validateConfirmation() {
        TextInputLayout confirm = findViewById(R.id.register_confirm);
        if (isConfirmationIncorrect()) {
            confirm.setError("Passwords must be identical");
            return false;
        } else {
            confirm.setError(null);
            confirm.setErrorEnabled(false);
            return true;
        }
    }

    private boolean isConfirmationIncorrect() {
        String password = inputPassword.getEditableText().toString();
        String confirm = inputConfirmPassword.getEditableText().toString();
        return !Objects.equals(password, confirm);
    }
}
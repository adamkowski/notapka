package com.gitlab.adamkowski.notapka.activities.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.auth0.android.jwt.DecodeException;
import com.auth0.android.jwt.JWT;
import com.gitlab.adamkowski.notapka.R;

public class UserSettingsActivity extends AppCompatActivity {
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);

        sharedPreferences = getSharedPreferences("user", Context.MODE_PRIVATE);

        TextView username = findViewById(R.id.settings_screen_tv_username);
        username.setText(getUsername());

        Button buttonLogout = findViewById(R.id.settings_screen_button_logout);
        buttonLogout.setOnClickListener(view -> logoutUser());
    }

    private String getUsername() {
        try {
            JWT token = new JWT(sharedPreferences.getString("token", ""));
            return token.getSubject();
        } catch (DecodeException e) {
            return "";
        }
    }

    private void logoutUser() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("token");
        editor.apply();
        finish();
    }
}
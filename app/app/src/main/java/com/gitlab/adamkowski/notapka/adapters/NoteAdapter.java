package com.gitlab.adamkowski.notapka.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gitlab.adamkowski.notapka.models.Note;
import com.gitlab.adamkowski.notapka.R;
import com.gitlab.adamkowski.notapka.interfaces.NoteAdapterOnClickHandler;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteAdapterViewHolder> {
    private final NoteAdapterOnClickHandler clickHandler;
    private Note[] notes;

    public NoteAdapter(NoteAdapterOnClickHandler clickHandler) {
        this.clickHandler = clickHandler;
    }

    public void setNotes(Note[] notes) {
        this.notes = notes;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NoteAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        int layoutIdForListItem = R.layout.note_item;
        View view = inflater.inflate(layoutIdForListItem, parent, false);
        return new NoteAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteAdapterViewHolder holder, int position) {
        Note noteOnScreen = this.notes[position];
        holder.listItemTitle.setText(noteOnScreen.getTitle());
        if (noteOnScreen.getContent() == null || noteOnScreen.getContent().trim().isEmpty()) {
            holder.listItemContent.setVisibility(View.GONE);
        } else {
            holder.listItemContent.setVisibility(View.VISIBLE);
            holder.listItemContent.setText(noteOnScreen.getContent());
        }
    }

    @Override
    public int getItemCount() {
        if (this.notes == null) {
            return 0;
        }
        return this.notes.length;
    }

    public class NoteAdapterViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        public final TextView listItemTitle;
        public final TextView listItemContent;

        public NoteAdapterViewHolder(View itemView) {
            super(itemView);

            this.listItemTitle = (TextView) itemView.findViewById(R.id.tv_item_note_title);
            this.listItemContent = (TextView) itemView.findViewById(R.id.tv_item_note_content);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            clickHandler.onClick(notes[adapterPosition]);
        }
    }
}

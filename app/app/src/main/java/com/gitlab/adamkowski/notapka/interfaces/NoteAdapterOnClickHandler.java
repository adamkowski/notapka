package com.gitlab.adamkowski.notapka.interfaces;

import com.gitlab.adamkowski.notapka.models.Note;

public interface NoteAdapterOnClickHandler {
    void onClick(Note oneNote);
}

package com.gitlab.adamkowski.notapka.models;

public class NoteDTO {
    private final String title;
    private final String content;

    public NoteDTO(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}

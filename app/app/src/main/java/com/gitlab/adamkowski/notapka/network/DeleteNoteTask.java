package com.gitlab.adamkowski.notapka.network;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import com.gitlab.adamkowski.notapka.utils.HttpConnection;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class DeleteNoteTask extends AsyncTask<URL, Void, Integer> {
    private final String token;
    private final WeakReference<Activity> weakActivity;

    public DeleteNoteTask(String token, Activity myActivity) {
        this.token = token;
        this.weakActivity = new WeakReference<>(myActivity);
    }

    @Override
    protected Integer doInBackground(URL... urls) {
        try {
            HttpURLConnection connection = null;
            try {
                connection = HttpConnection.make(HttpConnection.Method.DELETE, urls[0], token);
                return connection.getResponseCode();
            } finally {
                HttpConnection.close(connection);
            }
        } catch (IOException e) {
            return -1;
        }
    }

    @Override
    protected void onPostExecute(Integer responseCode) {
        switch (responseCode) {
            case HTTP_OK:
                displayMessage("Note successfully deleted");
                break;
            case HTTP_NO_CONTENT:
                displayMessage("Note with the specified id does not exist");
                break;
            case HTTP_UNAUTHORIZED:
                displayMessage("The user is unauthenticated for this operation");
                break;
            case HTTP_FORBIDDEN:
                displayMessage("The user tried to delete someone else's note");
                break;
            default:
                displayMessage("Connection error");
        }

        Activity activity = weakActivity.get();
        if (activity != null && !(activity.isFinishing() || activity.isDestroyed())) {
            if (responseCode == HttpURLConnection.HTTP_OK) {
                Toast.makeText(activity, "Note has been deleted", Toast.LENGTH_SHORT).show();
                activity.finish();
            } else {
                Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void displayMessage(String text) {
        Activity activity = weakActivity.get();
        if (isActivityActive(activity)) {
            Toast.makeText(activity, text, Toast.LENGTH_SHORT).show();
            activity.finish();
        }
    }

    private boolean isActivityActive(Activity activity) {
        return activity != null && !(activity.isFinishing() || activity.isDestroyed());
    }
}

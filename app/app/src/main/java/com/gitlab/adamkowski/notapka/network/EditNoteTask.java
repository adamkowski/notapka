package com.gitlab.adamkowski.notapka.network;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import com.gitlab.adamkowski.notapka.models.NoteDTO;
import com.gitlab.adamkowski.notapka.utils.HttpConnection;
import com.google.gson.Gson;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.writeHttpMessage;
import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

public class EditNoteTask extends AsyncTask<URL, Void, Integer> {
    private final String token;
    private final NoteDTO editedNote;
    private final WeakReference<Activity> weakActivity;

    public EditNoteTask(String token, NoteDTO note, Activity myActivity) {
        this.token = token;
        this.editedNote = note;
        weakActivity = new WeakReference<>(myActivity);
    }

    @Override
    protected Integer doInBackground(URL... urls) {
        int responseCode;
        try {
            HttpURLConnection connection = null;
            try {
                connection = HttpConnection.make(HttpConnection.Method.PUT, urls[0], token);
                writeHttpMessage(connection, new Gson().toJson(editedNote));
                responseCode = connection.getResponseCode();
            } finally {
                HttpConnection.close(connection);
            }
        } catch (IOException e) {
            return -1;
        }
        return responseCode;
    }

    @Override
    protected void onPostExecute(Integer responseCode) {
        switch (responseCode) {
            case HTTP_OK:
                displayMessage("Note updated successfully");
                break;
            case HTTP_NO_CONTENT:
                displayMessage("Note with the specified id does not exist");
                break;
            case HTTP_UNAUTHORIZED:
                displayMessage("The user is unauthenticated for this operation");
                break;
            case HTTP_FORBIDDEN:
                displayMessage("The user tried to edit someone else's note");
                break;
            default:
                displayMessage("Connection error");
        }
    }

    private void displayMessage(String text) {
        Activity activity = weakActivity.get();
        if (isActivityActive(activity)) {
            Toast.makeText(activity, text, Toast.LENGTH_SHORT).show();
            activity.finish();
        }
    }

    private boolean isActivityActive(Activity activity) {
        return activity != null && !(activity.isFinishing() || activity.isDestroyed());
    }
}

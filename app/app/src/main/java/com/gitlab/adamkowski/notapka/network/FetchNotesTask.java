package com.gitlab.adamkowski.notapka.network;

import android.os.AsyncTask;

import com.gitlab.adamkowski.notapka.adapters.NoteAdapter;
import com.gitlab.adamkowski.notapka.models.Note;
import com.gitlab.adamkowski.notapka.utils.HttpConnection;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.receiveHttpResponseMessage;

public class FetchNotesTask extends AsyncTask<URL, Void, Note[]> {
    private final NoteAdapter noteAdapter;
    private final String token;

    public FetchNotesTask(NoteAdapter noteAdapter, String token) {
        this.noteAdapter = noteAdapter;
        this.token = token;
    }

    @Override
    protected Note[] doInBackground(URL... urls) {
        String response;
        try {
            HttpURLConnection connection = null;
            try {
                connection = HttpConnection.make(HttpConnection.Method.GET, urls[0], token);
                response = receiveHttpResponseMessage(connection);
            } finally {
                HttpConnection.close(connection);
            }
        } catch (IOException e) {
            return new Note[]{};
        }
        return new Gson().fromJson(response, Note[].class);
    }

    @Override
    protected void onPostExecute(Note[] result) {
        if (result != null) {
            noteAdapter.setNotes(result);
        }
    }
}

package com.gitlab.adamkowski.notapka.network;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.Toast;

import com.gitlab.adamkowski.notapka.models.User;
import com.gitlab.adamkowski.notapka.utils.HttpConnection;
import com.google.gson.Gson;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.writeHttpMessage;

public class LoginUserTask extends AsyncTask<URL, Void, Response> {
    private final User user;
    private final WeakReference<Activity> weakActivity;
    private final SharedPreferences sharedPreferences;

    public LoginUserTask(User user, Activity activity, SharedPreferences sharedPreferences) {
        this.user = user;
        this.weakActivity = new WeakReference<>(activity);
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    protected Response doInBackground(URL... urls) {
        try {
            HttpURLConnection connection = null;
            try {
                connection = HttpConnection.make(HttpConnection.Method.POST, urls[0]);
                writeHttpMessage(connection, new Gson().toJson(user));
                return new Response(connection.getResponseCode(), connection.getHeaderField("Authorization"));
            } finally {
                HttpConnection.close(connection);
            }
        } catch (IOException exception) {
            return new Response(-1, "");
        }
    }

    @Override
    protected void onPostExecute(Response response) {
        Activity activity = weakActivity.get();
        if (isActivityActive(activity)) {
            switch (response.getResponseCode()) {
                case 200:
                    saveCurrentlyLoggedUser(response.getContent());
                    activity.finish();
                    break;
                case 401:
                    Toast.makeText(activity, "Invalid credentials", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    Toast.makeText(activity, "Login failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isActivityActive(Activity activity) {
        return activity != null && !(activity.isFinishing() || activity.isDestroyed());
    }

    private void saveCurrentlyLoggedUser(String token) {
        final String prefix = "Bearer ";
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("token", token.substring(prefix.length()));
        editor.apply();
    }
}

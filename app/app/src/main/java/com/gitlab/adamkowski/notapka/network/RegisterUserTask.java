package com.gitlab.adamkowski.notapka.network;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import com.gitlab.adamkowski.notapka.models.User;
import com.gitlab.adamkowski.notapka.utils.HttpConnection;
import com.google.gson.Gson;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.writeHttpMessage;

public class RegisterUserTask extends AsyncTask<URL, Void, Integer> {
    private final String sendMessage;
    private final WeakReference<Activity> weakActivity;

    public RegisterUserTask(User user, Activity myActivity) {
        this.sendMessage = new Gson().toJson(user);
        this.weakActivity = new WeakReference<>(myActivity);
    }

    @Override
    protected Integer doInBackground(URL... urls) {
        int responseCode;
        try {
            HttpURLConnection connection = null;
            try {
                connection = HttpConnection.make(HttpConnection.Method.POST, urls[0]);
                writeHttpMessage(connection, sendMessage);
                responseCode = connection.getResponseCode();
            } finally {
                HttpConnection.close(connection);
            }
        } catch (IOException exception) {
            responseCode = -1;
        }
        return responseCode;
    }

    @Override
    protected void onPostExecute(Integer responseMessage) {
        switch (responseMessage) {
            case 201:
                displayMessage("User successfully registered");
                break;
            case 400:
                displayMessage("Invalid user data");
                break;
            case 409:
                displayMessage("The specified user already exists");
                break;
            default:
                displayMessage("Connection error");
        }
    }

    private void displayMessage(String responseMessage) {
        Activity activity = weakActivity.get();
        if (activity != null && !(activity.isFinishing() || activity.isDestroyed())) {
            Toast.makeText(activity, responseMessage, Toast.LENGTH_SHORT).show();
            activity.finish();
        }
    }
}

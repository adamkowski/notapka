package com.gitlab.adamkowski.notapka.network;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import com.gitlab.adamkowski.notapka.models.NoteDTO;
import com.gitlab.adamkowski.notapka.utils.HttpConnection;
import com.google.gson.Gson;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.receiveHttpResponseMessage;
import static com.gitlab.adamkowski.notapka.utils.NetworkUtils.writeHttpMessage;

public class SaveNoteTask extends AsyncTask<URL, Void, Response> {
    private final NoteDTO note;
    private final String token;
    private final WeakReference<Activity> weakActivity;

    public SaveNoteTask(NoteDTO note, String token, Activity activity) {
        this.note = note;
        this.token = token;
        this.weakActivity = new WeakReference<>(activity);
    }

    @Override
    protected Response doInBackground(URL... urls) {
        try {
            HttpURLConnection connection = null;
            try {
                connection = HttpConnection.make(HttpConnection.Method.POST, urls[0], token);
                writeHttpMessage(connection, new Gson().toJson(note));
                String response = receiveHttpResponseMessage(connection);
                return new Response(connection.getResponseCode(), response);
            } finally {
                HttpConnection.close(connection);
            }
        } catch (IOException e) {
            return new Response(-1, "");
        }
    }

    @Override
    protected void onPostExecute(Response response) {
        switch (response.getResponseCode()) {
            case 201:
                displayMessage("Note created successfully");
                break;
            case 400:
                displayMessage("The note does not have the required fields");
                break;
            case 401:
                displayMessage("The user is unauthenticated for this operation");
                break;
            default:
                displayMessage("Connection error");
        }
    }

    private void displayMessage(String text) {
        Activity activity = weakActivity.get();
        if (isActivityActive(activity)) {
            Toast.makeText(activity, text, Toast.LENGTH_SHORT).show();
            activity.finish();
        }
    }

    private boolean isActivityActive(Activity activity) {
        return activity != null && !(activity.isFinishing() || activity.isDestroyed());
    }
}
package com.gitlab.adamkowski.notapka.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpConnection {
    public enum Method {
        GET, POST, PUT, DELETE
    }

    private HttpConnection() {
    }

    public static HttpURLConnection make(Method method, URL url) throws IOException {
        HttpURLConnection connection = makeConnection(url);
        connection.setRequestMethod(method.name());
        connection.setDoOutput(method == Method.POST || method == Method.PUT);
        return connection;
    }

    public static HttpURLConnection make(Method method, URL url, String token) throws IOException {
        HttpURLConnection connection = make(method, url);
        connection.setRequestProperty("Authorization", "Bearer " + token);
        return connection;
    }

    public static void close(HttpURLConnection connection) {
        if (connection != null) {
            connection.disconnect();
        }
    }

    private static HttpURLConnection makeConnection(URL url) throws IOException {
        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Content-Type", "application/json; utf-8");
            connection.setRequestProperty("Accept", "application/json");
            return connection;
        } catch (IOException e) {
            throw new IOException("Server error. Please try again later");
        }
    }
}

package com.gitlab.adamkowski.notapka.utils;

import android.net.Uri;

import com.gitlab.adamkowski.notapka.BuildConfig;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NetworkUtils {
    public static final String API_SERVER_ADDRESS = BuildConfig.API_URL;
    public static final String API_REGISTER = "register";
    public static final String API_LOGIN = "login";
    public static final String API_NOTES = "notes";

    private static final Set<Integer> SUCCESS_RESPONSE_CODES = Stream.of(
            HttpURLConnection.HTTP_OK,
            HttpURLConnection.HTTP_CREATED)
            .collect(Collectors.toSet());

    private NetworkUtils() {
    }

    public static String receiveHttpResponseMessage(HttpURLConnection connection) throws IOException {
        try (InputStream messageStream = receiveMessageStream(connection)) {
            return readMessageFromStream(messageStream);
        }
    }

    private static InputStream receiveMessageStream(HttpURLConnection conn) throws IOException {
        if (SUCCESS_RESPONSE_CODES.contains(conn.getResponseCode())) {
            return conn.getInputStream();
        }
        return conn.getErrorStream();
    }

    private static String readMessageFromStream(InputStream is) throws IOException {
        try (InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
             BufferedReader messageReader = new BufferedReader(isr)) {
            return messageReader.lines()
                    .collect(Collectors.joining());
        } catch (IOException e) {
            throw new IOException("Can't read server response");
        }
    }

    public static void writeHttpMessage(HttpURLConnection conn, String message) throws IOException {
        try (OutputStream outputStream = new BufferedOutputStream(conn.getOutputStream())) {
            outputStream.write(message.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new IOException("Server error. Please try again later");
        }
    }

    public static URL makeUrl(String host, String... path) {
        StringBuilder address = new StringBuilder(host);
        Arrays.stream(path).forEach(part -> address.append("/").append(part));
        try {
            return new URL(address.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }
}

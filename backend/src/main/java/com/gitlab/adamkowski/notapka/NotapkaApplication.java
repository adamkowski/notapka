package com.gitlab.adamkowski.notapka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotapkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotapkaApplication.class, args);
    }

}

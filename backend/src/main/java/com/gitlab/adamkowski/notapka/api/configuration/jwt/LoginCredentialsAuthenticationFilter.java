package com.gitlab.adamkowski.notapka.api.configuration.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.adamkowski.notapka.api.exception.type.MalformedLoginQueryException;
import com.gitlab.adamkowski.notapka.api.model.dto.LoginCredentialsDto;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

public class LoginCredentialsAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private final ObjectMapper objectMapper;

    public LoginCredentialsAuthenticationFilter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {
        try {
            final String jsonContent = request.getReader().lines()
                    .collect(Collectors.joining());

            LoginCredentialsDto authRequest = objectMapper.readValue(jsonContent, LoginCredentialsDto.class);
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    authRequest.getUsername(),
                    authRequest.getPassword()
            );

            setDetails(request, token);
            return getAuthenticationManager().authenticate(token);
        } catch (IOException exception) {
            throw new MalformedLoginQueryException(exception.getMessage());
        }
    }
}

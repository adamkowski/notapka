package com.gitlab.adamkowski.notapka.api.controller;

import com.gitlab.adamkowski.notapka.api.model.dto.LoginCredentialsDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "2. Login")
@RestController
@RequestMapping(path = "/api/v3")
public class LoginController {

    @Operation(summary = "Returns the JWT token to the authenticated user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User successfully authenticated"),
            @ApiResponse(responseCode = "401", description = "Invalid credentials")
    })
    @PostMapping("/login")
    public void login(@RequestBody LoginCredentialsDto credentials) {
    }
}

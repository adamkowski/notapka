package com.gitlab.adamkowski.notapka.api.controller;

import com.gitlab.adamkowski.notapka.api.model.dto.NoteDto;
import com.gitlab.adamkowski.notapka.api.model.entity.Note;
import com.gitlab.adamkowski.notapka.api.service.NoteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Tag(name = "3. Notes")
@RestController
@RequestMapping(path = "/api/v3/notes")
public class NoteRestController {
    private final NoteService noteService;

    public NoteRestController(NoteService noteService) {
        this.noteService = noteService;
    }

    @Operation(summary = "The authenticated user retrieves all his notes", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Data downloaded successfully"),
            @ApiResponse(responseCode = "401", description = "The user is unauthenticated for this operation", content = @Content)
    })
    @GetMapping
    public ResponseEntity<List<Note>> fetchNotes(Principal user) {
        return ResponseEntity.ok(noteService.fetchUserNotes(user.getName()));
    }

    @Operation(summary = "The user saves a new note to the database", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Note created successfully"),
            @ApiResponse(responseCode = "400", description = "The note does not have the required fields"),
            @ApiResponse(responseCode = "401", description = "The user is unauthenticated for this operation", content = @Content)
    })
    @PostMapping
    public ResponseEntity<String> saveNote(@RequestBody @Valid NoteDto note,
                                           Principal user) {
        noteService.saveNote(note, user.getName());
        return new ResponseEntity<>("Note has been saved", HttpStatus.CREATED);
    }

    @Operation(summary = "The user updates the note that he owns", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Note updated successfully"),
            @ApiResponse(responseCode = "204", description = "Note with the specified id does not exist", content = @Content),
            @ApiResponse(responseCode = "401", description = "The user is unauthenticated for this operation", content = @Content),
            @ApiResponse(responseCode = "403", description = "The user tried to edit someone else's note", content = @Content)
    })
    @PutMapping(path = "/{noteId}")
    public ResponseEntity<Note> updateNote(@PathVariable Long noteId,
                                           @RequestBody @Valid NoteDto updatedNote,
                                           Principal user) {
        Note updated = noteService.updateNote(noteId, updatedNote, user.getName());
        return ResponseEntity.ok(updated);
    }

    @Operation(summary = "The user deletes the note that he owns", security = @SecurityRequirement(name = "bearerAuth"))
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Note successfully deleted"),
            @ApiResponse(responseCode = "204", description = "Note with the specified id does not exist"),
            @ApiResponse(responseCode = "401", description = "The user is unauthenticated for this operation"),
            @ApiResponse(responseCode = "403", description = "The user tried to delete someone else's note")
    })
    @DeleteMapping(path = "/{noteId}")
    public ResponseEntity<Void> deleteNote(@PathVariable Long noteId,
                                           Principal user) {
        noteService.deleteNote(noteId, user.getName());
        return ResponseEntity.ok().build();
    }
}

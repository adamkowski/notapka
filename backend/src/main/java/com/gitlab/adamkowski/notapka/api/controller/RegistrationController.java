package com.gitlab.adamkowski.notapka.api.controller;

import com.gitlab.adamkowski.notapka.api.model.dto.UserDto;
import com.gitlab.adamkowski.notapka.api.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Tag(name = "1. Registration")
@RestController
@RequestMapping(path = "/api/v3")
public class RegistrationController {
    private final UserService userService;

    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @Operation(summary = "Register new application user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "User successfully registered", content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid user data"),
            @ApiResponse(responseCode = "409", description = "The specified user already exists", content = @Content)
    })
    @PostMapping(path = "/register")
    public ResponseEntity<String> registerNewUser(@Valid @RequestBody UserDto user) {
        userService.registerNewUser(user);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}

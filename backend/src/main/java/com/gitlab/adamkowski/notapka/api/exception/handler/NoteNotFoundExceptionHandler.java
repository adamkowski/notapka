package com.gitlab.adamkowski.notapka.api.exception.handler;

import com.gitlab.adamkowski.notapka.api.exception.type.NoteNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class NoteNotFoundExceptionHandler {

    @ExceptionHandler(value = NoteNotFoundException.class)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> handleNonExistentNote() {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

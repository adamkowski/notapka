package com.gitlab.adamkowski.notapka.api.exception.handler;

import com.gitlab.adamkowski.notapka.api.exception.type.UserIsNotNoteOwner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class UserIsNotNoteOwnerExceptionHandler {

    @ExceptionHandler(value = UserIsNotNoteOwner.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ResponseEntity<Void> handleEditingSomeoneElseNote() {
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
}

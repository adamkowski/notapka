package com.gitlab.adamkowski.notapka.api.exception.type;

import org.springframework.security.core.AuthenticationException;

public class MalformedLoginQueryException extends AuthenticationException {

    public MalformedLoginQueryException(String msg) {
        super(msg);
    }
}

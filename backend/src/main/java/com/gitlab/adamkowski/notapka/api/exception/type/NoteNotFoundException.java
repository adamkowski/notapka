package com.gitlab.adamkowski.notapka.api.exception.type;

public class NoteNotFoundException extends RuntimeException {
    public NoteNotFoundException() {
    }

    public NoteNotFoundException(String message) {
        super(message);
    }
}

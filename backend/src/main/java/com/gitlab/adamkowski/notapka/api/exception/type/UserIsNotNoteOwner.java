package com.gitlab.adamkowski.notapka.api.exception.type;

public class UserIsNotNoteOwner extends RuntimeException {
    public UserIsNotNoteOwner() {
    }

    public UserIsNotNoteOwner(String message) {
        super(message);
    }
}

package com.gitlab.adamkowski.notapka.api.model.dto;

public class LoginCredentialsDto {
    private String username;
    private String password;

    public LoginCredentialsDto() {
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}

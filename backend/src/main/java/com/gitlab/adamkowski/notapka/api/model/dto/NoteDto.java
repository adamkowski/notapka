package com.gitlab.adamkowski.notapka.api.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class NoteDto {
    @NotBlank(message = "Title must be provided")
    @Size(min = 1, max = 64, message = "title must be {min} to {max} characters long")
    private String title;
    private String content;

    public NoteDto() {
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}

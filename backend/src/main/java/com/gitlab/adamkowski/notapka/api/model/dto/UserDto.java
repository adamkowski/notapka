package com.gitlab.adamkowski.notapka.api.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UserDto {
    @NotBlank(message = "Username must be provided")
    @Size(min = 6, max = 32, message = "Username must be {min} to {max} characters long")
    private String username;

    @NotBlank(message = "Password must be provided")
    @Size(min = 6, max = 16, message = "Password must be {min} to {max} characters long")
    private String password;

    public UserDto() {
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}

package com.gitlab.adamkowski.notapka.api.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gitlab.adamkowski.notapka.api.model.dto.NoteDto;

import javax.persistence.*;

@JsonIgnoreProperties({"user"})
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class Note {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String content;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Note() {
    }

    public Note(NoteDto note, User user) {
        this.title = note.getTitle();
        this.content = note.getContent();
        this.user = user;
    }

    public void update(NoteDto update) {
        title = update.getTitle();
        content = update.getContent();
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public User getUser() {
        return user;
    }
}

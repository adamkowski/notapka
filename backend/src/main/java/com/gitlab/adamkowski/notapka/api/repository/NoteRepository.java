package com.gitlab.adamkowski.notapka.api.repository;

import com.gitlab.adamkowski.notapka.api.model.entity.Note;
import com.gitlab.adamkowski.notapka.api.model.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NoteRepository extends CrudRepository<Note, Long> {
    List<Note> getNotesByUser(User user);
}

package com.gitlab.adamkowski.notapka.api.repository;

import com.gitlab.adamkowski.notapka.api.model.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    boolean existsUserByUsername(String username);

    Optional<User> findUserByUsername(String username);
}

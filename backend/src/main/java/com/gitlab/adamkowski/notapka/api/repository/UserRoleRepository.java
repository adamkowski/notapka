package com.gitlab.adamkowski.notapka.api.repository;

import com.gitlab.adamkowski.notapka.api.model.entity.UserRole;
import org.springframework.data.repository.CrudRepository;

public interface UserRoleRepository extends CrudRepository<UserRole, Long> {
}

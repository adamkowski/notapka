package com.gitlab.adamkowski.notapka.api.service;

import com.gitlab.adamkowski.notapka.api.exception.type.NoteNotFoundException;
import com.gitlab.adamkowski.notapka.api.exception.type.UserIsNotNoteOwner;
import com.gitlab.adamkowski.notapka.api.model.dto.NoteDto;
import com.gitlab.adamkowski.notapka.api.model.entity.Note;
import com.gitlab.adamkowski.notapka.api.model.entity.User;
import com.gitlab.adamkowski.notapka.api.repository.NoteRepository;
import com.gitlab.adamkowski.notapka.api.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
public class NoteService {
    private final NoteRepository noteRepository;
    private final UserRepository userRepository;

    public NoteService(NoteRepository noteRepository, UserRepository userRepository) {
        this.noteRepository = noteRepository;
        this.userRepository = userRepository;
    }

    public List<Note> fetchUserNotes(String username) {
        return userRepository.findUserByUsername(username)
                .map(noteRepository::getNotesByUser)
                .orElse(Collections.emptyList());
    }

    public void saveNote(NoteDto note, String ownerMail) {
        userRepository.findUserByUsername(ownerMail)
                .ifPresent(user -> noteRepository.save(new Note(note, user)));
    }

    public Note updateNote(Long noteId, NoteDto changes, String username) {
        Note note = noteRepository.findById(noteId).orElseThrow(NoteNotFoundException::new);
        checkOwner(note, username);
        note.update(changes);
        return noteRepository.save(note);
    }

    public void deleteNote(Long noteId, String username) {
        Note note = noteRepository.findById(noteId).orElseThrow(NoteNotFoundException::new);
        checkOwner(note, username);
        noteRepository.delete(note);
    }

    private void checkOwner(Note note, String username) {
        User user = userRepository.findUserByUsername(username).orElse(null);
        if (!Objects.equals(note.getUser(), user)) {
            throw new UserIsNotNoteOwner();
        }
    }
}

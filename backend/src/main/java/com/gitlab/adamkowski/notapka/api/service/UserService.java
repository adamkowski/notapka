package com.gitlab.adamkowski.notapka.api.service;

import com.gitlab.adamkowski.notapka.api.exception.type.UserAlreadyExistsException;
import com.gitlab.adamkowski.notapka.api.model.dto.UserDto;
import com.gitlab.adamkowski.notapka.api.model.entity.User;
import com.gitlab.adamkowski.notapka.api.model.entity.UserRole;
import com.gitlab.adamkowski.notapka.api.repository.UserRepository;
import com.gitlab.adamkowski.notapka.api.repository.UserRoleRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;
    private final PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository,
                       UserRoleRepository userRoleRepository,
                       PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void registerNewUser(UserDto user) {
        if (userRepository.existsUserByUsername(user.getUsername())) {
            throw new UserAlreadyExistsException();
        }

        User newUser = new User(user.getUsername(), passwordEncoder.encode(user.getPassword()));
        userRepository.save(newUser);

        UserRole userRole = new UserRole(newUser.getUsername(), "ROLE_USER");
        userRoleRepository.save(userRole);
    }
}

package com.gitlab.adamkowski.notapka;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@DisplayName("Application")
class NotapkaApplicationTests {

    @Test
    @DisplayName("Load context")
    void contextLoads() {
    }

}

package com.gitlab.adamkowski.notapka.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@DisplayName("User authorization")
@AutoConfigureMockMvc
@Transactional
class LoginControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @DisplayName("Should return a token for a correctly authenticated user")
    void shouldReturnATokenForACorrectlyAuthenticatedUser() throws Exception {
        ObjectNode user = objectMapper.createObjectNode();
        user.put("username", "test_user_1@test.asd");
        user.put("password", "password");

        MockHttpServletRequestBuilder registerUserRequest = post("/api/v3/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user));

        MvcResult result = mockMvc.perform(registerUserRequest)
                .andExpect(status().isOk())
                .andReturn();

        String token = result.getResponse().getHeader("Authorization");
        assertNotNull(token);
        assertTrue(token.startsWith("Bearer"));
    }

    @Test
    @DisplayName("Should return unauthorized for invalid login credentials")
    void shouldReturnUnauthorizedForInvalidLoginCredentials() throws Exception {
        ObjectNode user = objectMapper.createObjectNode();
        user.put("username", "test_user_1@test.asd");
        user.put("password", "wrong_password");

        MockHttpServletRequestBuilder registerUserRequest = post("/api/v3/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user));

        MvcResult result = mockMvc.perform(registerUserRequest)
                .andExpect(status().isUnauthorized())
                .andReturn();

        String token = result.getResponse().getHeader("Authorization");
        assertNull(token);
    }

    @Test
    @DisplayName("Should handle an invalid login query")
    void shouldHandleAnInvalidLoginQuery() throws Exception {
        String malformedJson = "{"
                + "\"username\": \"test_user_1@test.asd\"" // No comma separator
                + "\"password\": \"password\""
                + "}";

        MockHttpServletRequestBuilder registerUserRequest = post("/api/v3/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(malformedJson);

        MvcResult result = mockMvc.perform(registerUserRequest)
                .andExpect(status().isUnauthorized())
                .andReturn();

        String token = result.getResponse().getHeader("Authorization");
        assertNull(token);
    }
}

package com.gitlab.adamkowski.notapka.api.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@DisplayName("Notes CRUD")
@AutoConfigureMockMvc
class NoteRestControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @DisplayName("Should return the notes belonging to the logged in user")
    @WithMockUser(username = "test_user_1@test.asd")
    void shouldReturnTheNotesBelongingToTheLoggedInUser() throws Exception {
        MockHttpServletRequestBuilder fetchNotes = get("/api/v3/notes")
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(fetchNotes)
                .andExpect(status().isOk())
                .andReturn();

        JsonNode node = objectMapper.readTree(result.getResponse().getContentAsString());
        String title = node.get(0).get("title").textValue();
        assertEquals("Test note with content", title);
    }

    @Test
    @DisplayName("Should not return the notes to unauthorized user")
    void shouldNotReturnTheNotesToUnauthorizedUser() throws Exception {
        MockHttpServletRequestBuilder fetchNotes = get("/api/v3/notes")
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(fetchNotes)
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("Should save a note with the correct fields")
    @WithMockUser(username = "test_user_1@test.asd")
    @Transactional
    void shouldSaveANoteWithTheCorrectFields() throws Exception {
        ObjectNode note = objectMapper.createObjectNode();
        note.put("title", "Title");
        note.put("content", "Content");

        MockHttpServletRequestBuilder saveNewNoteRequest = post("/api/v3/notes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(note));

        mockMvc.perform(saveNewNoteRequest)
                .andExpect(status().isCreated());
    }

    @Test
    @DisplayName("Should prevent to save the note without a title")
    @WithMockUser(username = "test_user_1@test.asd")
    void shouldPreventToSaveTheNoteWithoutATitle() throws Exception {
        ObjectNode note = objectMapper.createObjectNode();
        note.put("content", "Content");

        MockHttpServletRequestBuilder saveNewNoteRequest = post("/api/v3/notes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(note));

        mockMvc.perform(saveNewNoteRequest)
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Should not save the notes of unauthorized user")
    void shouldNotSaveTheNotesOfUnauthorizedUser() throws Exception {
        ObjectNode note = objectMapper.createObjectNode();
        note.put("title", "Title");
        note.put("content", "Content");

        MockHttpServletRequestBuilder saveNewNoteRequest = post("/api/v3/notes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(note));

        mockMvc.perform(saveNewNoteRequest)
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("Should update the user's note")
    @Transactional
    @WithMockUser(username = "test_user_1@test.asd")
    void shouldUpdateTheUserNote() throws Exception {
        ObjectNode note = objectMapper.createObjectNode();
        note.put("title", "Title");
        note.put("content", "Content");

        MockHttpServletRequestBuilder updateNoteRequest = put("/api/v3/notes/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(note));

        MvcResult result = mockMvc.perform(updateNoteRequest)
                .andExpect(status().isOk())
                .andReturn();

        JsonNode node = objectMapper.readTree(result.getResponse().getContentAsString());
        assertEquals("Title", node.get("title").textValue());
        assertEquals("Content", node.get("content").textValue());
    }

    @Test
    @DisplayName("Should prevent a nonexistent note from being updated")
    @WithMockUser(username = "test_user_1@test.asd")
    void shouldPreventANonexistentNoteFromBeingUpdated() throws Exception {
        ObjectNode note = objectMapper.createObjectNode();
        note.put("title", "Title");
        note.put("content", "Content");

        MockHttpServletRequestBuilder updateNoteRequest = put("/api/v3/notes/0") // Id doesn't exist
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(note));

        mockMvc.perform(updateNoteRequest)
                .andExpect(status().isNoContent());
    }

    @Test
    @DisplayName("Should prevent updating another user's note")
    @WithMockUser(username = "test_user_1@test.asd")
    void shouldPreventUpdatingAnotherUserNote() throws Exception {
        ObjectNode note = objectMapper.createObjectNode();
        note.put("title", "Title");
        note.put("content", "Content");

        MockHttpServletRequestBuilder updateNoteRequest = put("/api/v3/notes/3") // test_user_2's note
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(note));

        mockMvc.perform(updateNoteRequest)
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("Should not update the note of unauthorized user")
    void shouldNotUpdateTheNoteOfUnauthorizedUser() throws Exception {
        ObjectNode note = objectMapper.createObjectNode();
        note.put("title", "Title");
        note.put("content", "Content");

        MockHttpServletRequestBuilder updateNoteRequest = put("/api/v3/notes/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(note));

        mockMvc.perform(updateNoteRequest)
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("Should delete the user's note")
    @Transactional
    @WithMockUser(username = "test_user_1@test.asd")
    void shouldDeleteTheUserNote() throws Exception {
        MockHttpServletRequestBuilder deleteNoteRequest = delete("/api/v3/notes/1")
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(deleteNoteRequest)
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Should prevent a nonexistent note from being deleted")
    @WithMockUser(username = "test_user_1@test.asd")
    void shouldPreventANonexistentNoteFromBeingDeleted() throws Exception {
        MockHttpServletRequestBuilder deleteNoteRequest = delete("/api/v3/notes/0") // Id doesn't exist
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(deleteNoteRequest)
                .andExpect(status().isNoContent());
    }

    @Test
    @DisplayName("Should prevent deleting another user's note")
    @WithMockUser(username = "test_user_1@test.asd")
    void shouldPreventDeletingAnotherUserNote() throws Exception {
        MockHttpServletRequestBuilder deleteNoteRequest = delete("/api/v3/notes/3") // test_user_2's note
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(deleteNoteRequest)
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("Should not delete the note of unauthorized user")
    void shouldNotDeleteTheNoteOfUnauthorizedUser() throws Exception {
        MockHttpServletRequestBuilder deleteNoteRequest = delete("/api/v3/notes/1")
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(deleteNoteRequest)
                .andExpect(status().isUnauthorized());
    }
}

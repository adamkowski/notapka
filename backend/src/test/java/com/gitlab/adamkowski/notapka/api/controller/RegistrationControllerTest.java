package com.gitlab.adamkowski.notapka.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@DisplayName("User registration")
@AutoConfigureMockMvc
@Transactional
class RegistrationControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @DisplayName("Should register a new user with correctly provided data")
    void shouldRegisterANewUserWithCorrectlyProvidedData() throws Exception {
        ObjectNode user = objectMapper.createObjectNode();
        user.put("username", "test_user");
        user.put("password", "test_password");

        MockHttpServletRequestBuilder registerUserRequest = post("/api/v3/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(user));

        mockMvc.perform(registerUserRequest)
                .andExpect(status().isCreated());
    }

    @Test
    @DisplayName("Should prevent registering a user with incomplete data")
    void shouldPreventRegisteringAUserWithIncompleteData() throws Exception {
        ObjectNode incompleteUser = objectMapper.createObjectNode();
        incompleteUser.put("username", "test_user");

        MockHttpServletRequestBuilder registerUserRequest = post("/api/v3/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(incompleteUser));

        mockMvc.perform(registerUserRequest)
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Should prevent registering a user with an existing name in the database")
    void shouldPreventRegisteringAUserWithAnExistingNameInTheDatabase() throws Exception {
        ObjectNode duplicatedUser = objectMapper.createObjectNode();
        duplicatedUser.put("username", "test_user_1@test.asd");
        duplicatedUser.put("password", "test_password");

        MockHttpServletRequestBuilder registerUserRequest = post("/api/v3/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(duplicatedUser));

        mockMvc.perform(registerUserRequest)
                .andExpect(status().isConflict());
    }
}
